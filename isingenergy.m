function energy = isingenergy(grid,J,h)

energy = -(0.5*transpose(grid)*J*grid+transpose(h)*grid);

end
