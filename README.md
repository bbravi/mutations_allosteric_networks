# Mutations_Allosteric_Networks

Run fitness_cost.m to compute mutation costs and epistasis in terms of cooperative fitness

Run inferred_costs.m to compute mutation costs and epistasis in terms of inferred energy

comppcost3.m, comppcost3PB.m, isingenergy.m, rmvtsrotPB.m are auxiliary functions called by these scripts

COOP_L12_Nsp360 folder contains:
- a multiple sequence alignment (MSA) of allosteric cooperative networks evolved in silico via the numerical scheme available at https://github.com/lyyanlely/AllostericMatter;
- a set of parameters (J, h) inferred by the ACE routine (available at https://github.com/johnbarton/ACE) with regularization L2 = 1/M where M is the size of the MSA.


Permission is granted for anyone to copy, use, or modify this software and accompanying documents for any uncommercial purposes, provided the publication:
Bravi, Ravasio, Brito, Wyart, Direct Coupling Analysis of Epistasis in Allosteric Materials, biorxiv 10.1101/519116 (2019)
is cited. This software and documents are distributed without any warranty, express or implied.

