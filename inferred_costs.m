%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This script takes as input a set of allosteric cooperative configurations, 
% the fitness mutation costs computed by fitness_cost.m and the inferred  
% parameters, it estimates swap mutation costs in the inferred model averaged 
% over configurations and compares them to fitness mutation costs

% Reference: Bravi, Ravasio, Brito, Wyart, Direct Coupling Analysis of Epistasis in Allosteric Materials, biorxiv 10.1101/519116 (2019) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


print_files = 1; % to print the mutation costs in files
Minit = 1; % start configuration to estimate fitness mutation costs
Mend = 5; % end configuration

h = importdata('COOP_L12_Nsp360/Inferred_parameters/shear_msa_402_PB-h.dat'); % Parameters inferred by ACE+QLS with L2 regularization = 1/M, where M is the size of the MSA
J = importdata('COOP_L12_Nsp360/Inferred_parameters/shear_msa_402_PB-J.dat');

data = load('COOP_L12_Nsp360/shear_msa_PB.dat'); % MSA of shear coop configurations of size M 
config = [data(:,1:12), data(:,14:15), data(:,17:18), data(:, 20:400), data(:, 404:408)]; % Removal of fully conserved sites, for which no parameter is inferred
N = 402;

M_dde=[];
M_dde0=[];
M_ddf0=[];

for cc = Minit:Mend,

xname = sprintf('%d', cc);
dirc = 'COOP_L12_Nsp360/Mutation_costs/';
name2=[dirc, 'Double_costs_12_12_360_' xname '.dat'];

if (exist(name2, 'file') ~= 0), 
   av_cost_double = importdata(name2);
else
    disp('File not found')
    return
end 
% convert fitness mutation costs to a matrix
firstposition = av_cost_double(:, 1);
secondposition = av_cost_double(:, 2);
listj = av_cost_double(:,3);
av_cost_double_mat=zeros(N,N);
h1 = 0;
h3 = 1;
for i = 1:(N - 1)
  for j = (i + 1): N
  av_cost_double_mat(i, j) = listj(h3 + (j - (i + 1)),1);
 end
 h1 = N - i;
 h3 = h3 + h1;
end 
fitness_mat = (av_cost_double_mat + transpose(av_cost_double_mat)); % matrix of fitness double mutation costs

grid = 1-config(cc,:); % Need to flip configurations for the gauge of parameters
gridp = grid(:); 

dde=zeros(N,N);
dde0=zeros(N,N);
ddf0=zeros(N,N);


d_dde=[];
d_ddf0=[];
d_dde0=[];

listpos = importdata('COOP_L12_Nsp360/listpos.dat'); % list of link positions in decreasing order of importance for mutation costs, to identify neutral positions

for r=1:N-1,
 for s=(r+1):N,
   swap_pos_ref1=0;
   swap_pos_ref2=0;  
 
  if (r ~= s) && ((grid(r) == 0 && grid(s)==1) || (grid(s)==0 && grid(r)==1)), % mutations costs are limited to swap movements

    for t=1:length(listpos)    
     if (gridp(listpos(t)) == 1 - gridp(r)) && (listpos(t) ~= r),
       pos_ref1 = listpos(t);
      break 
     end
    end

    for t=1:length(listpos)    
     if (gridp(listpos(t)) == 1 - gridp(s)) && (listpos(t) ~= s) && (listpos(t) ~= pos_ref1),
       pos_ref2 = listpos(t);
      break
     end
    end 

    gridp_temp = gridp;
    gridp_temp(s) = 1-gridp(s);
    gridp_temp(r) = 1-gridp(r);
    dde0(r,s) = isingenergy(gridp_temp,J,h) - isingenergy(gridp,J,h);
  
    gridpp = gridp;
    gridpp(r) = gridp(pos_ref1);
    gridpp(pos_ref1) = gridp(r);
    swap_pos_ref1 = isingenergy(gridpp,J,h) - isingenergy(gridp,J,h); % effective single site cost in the inferred model

    gridpp = gridp;
    gridpp(s) = gridp(pos_ref2);
    gridpp(pos_ref2) = gridp(s);
    swap_pos_ref2=isingenergy(gridpp,J,h)-isingenergy(gridp,J,h);
  
    dde(r,s) = dde0(r,s) - swap_pos_ref1 - swap_pos_ref2;
    ddf0(r,s) = fitness_mat(r,s);

  
 end
  
   d_dde=[d_dde; dde(r,s)]; % epistasis in the inferred model
   d_dde0=[d_dde0; dde0(r,s)]; % double mutation costs in the inferred model
   d_ddf0=[d_ddf0; ddf0(r,s)]; % double fitness mutation costs

 end
end

% group mutation costs and average over Mend-Minit configurations
M_dde = [M_dde, d_dde];
M_dde0 = [M_dde0, d_dde0];
M_ddf0 = [M_ddf0, d_ddf0];

end

mat1 = M_dde;
mat2 = M_dde0;
mat3 = M_ddf0;

M_index=[];
for r=1:N-1,
 for s=r+1:N,
  M_index = [M_index; r, s];
 end
end 

L1=length(mat1(1,:));
L2=length(mat2(1,:));
L3=length(mat3(1,:));
NZ1=zeros(length(mat1),1);
NZ2=zeros(length(mat2),1);
NZ3=zeros(length(mat3),1);

mean_dde = zeros(length(mat1), 3);  
mean_dde0 = zeros(length(mat2), 3); 
mean_ddef0 = zeros(length(mat3), 3); 

for i=1:length(mat1)
 for j=1:L1
   if mat1(i,j)~=0 
     NZ1(i,1)=NZ1(i,1)+1;
   end
 end
if NZ1(i,1) ~= 0
 mean_dde(i,3) = sum(mat1(i,:))/NZ1(i,1);
 % 3rd column is epistasis in the inferred model averaged over m = Mend-Minit configurations
end
 mean_dde(i,1) = M_index(i,1);
 % 1st column is the index of the first link
 mean_dde(i,2) = M_index(i,2);
 % 2nd column is the index of the second link
end

for i=1:length(mat2)
 for j=1:L2
   if mat2(i,j)~=0 
     NZ2(i,1)=NZ2(i,1)+1;
   end
 end
if NZ2(i,1) ~= 0
 mean_dde0(i,3) = sum(mat2(i,:))/NZ2(i,1); 
 % 3rd column are double mutation costs in the inferred model averaged over m configurations
end
 mean_dde0(i,1) = M_index(i,1);
 mean_dde0(i,2) = M_index(i,2);
end

for i=1:length(mat3)
 for j=1:L3
   if mat3(i,j)~=0 
     NZ3(i,1)=NZ3(i,1)+1;
   end
 end
if NZ3(i,1) ~= 0
 mean_ddf0(i,3) = sum(mat3(i,:))/NZ3(i,1);
 % 3rd column are double fitness mutation costs averaged over m configurations
end
 mean_ddf0(i,1) = M_index(i,1);
 mean_ddf0(i,2) = M_index(i,2);
end


if print_files == 1, % print average double mutation costs and epistasis 
	file1nm = 'average_inferred_epistasis';
	file2nm = 'average_inferred_double_mutations';
	file3nm = 'average_fitness_double_mutations';
	FILE1 = [dirc,file1nm '.dat'];
	FILE2 = [dirc,file2nm '.dat'];
	FILE3 = [dirc,file3nm '.dat'];
	dlmwrite(FILE1,mean_dde,'\t')
	dlmwrite(FILE2,mean_dde0,'\t')
	dlmwrite(FILE3,mean_ddf0,'\t')

end
